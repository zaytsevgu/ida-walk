Simple library for idapython.

Main goal is fight with situations when all strings in binary are somehow
obfuscated and decoded by calling some function.

Walker is collecting all calls
to such function and also collect arguments for it. So, you just need to implement
decoding algo and apply it to list which will be returned by Walker.




Moreover it can be used not only for strings decoding. For example file objSend.py
make a comment with a selector at every call to _objc_msgSend method:


```
__text:0000000100011DD7                 call    cs:_objc_msgSend_ptr ; statusMsg
__text:0000000100011DDD                 mov     rdi, rax
__text:0000000100011DE0                 call    _objc_retainAutoreleasedReturnValue
__text:0000000100011DE5                 mov     [rbp+var_128], rax
__text:0000000100011DEC                 mov     r15, cs:classRef_NSString
__text:0000000100011DF3                 mov     rsi, cs:selRef_item
__text:0000000100011DFA                 mov     rdi, r14
__text:0000000100011DFD                 call    cs:_objc_msgSend_ptr ; item
__text:0000000100011E03                 mov     rdi, rax
__text:0000000100011E06                 call    _objc_retainAutoreleasedReturnValue
```

It can be easily extended by creating own handlers for assembly instructions. For now only x86/x64 and mips is supported

BPArgsTracker is one of examples such extending handler. It tracks movs at local variables and allows to create hooks for functions. This can be usefull if we works with C++ strings:
```
std::string s("obfuscated");
s.decode(); // s - now original string
```
We can add hook at std::string constructor so, at the moment of decode call we have connection beetween hardcoded obfustated string and place where decode is taken

```
mov rsi, dword_12345 
lea rdi, [rbp-800]
call std::string
...
lea rdi, [rbp-800]
call decode
```

Another example of usage: we've got a executable that always make a library call in a such manner:
```
push <hash>
...
call getFunction
...
call eax
```
Using Walker one can collect all such call's to getFunction and resolve it somehow. I use it for samples that saves result of getFunction in global vars and call them later.
So, I create script that automatically rename such variables according returned value of getFunction