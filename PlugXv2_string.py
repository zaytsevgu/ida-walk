import idaapi
import idautils
import idc
import Walker
idaapi.require("Walker")
idaapi.require("Walker.Walker")
idaapi.require("Walker.Handlers")
idaapi.require("Walker.x86Handlers")
from array import array
"""
Example for decoding strings in PlugX v2
Just call getAndCall with name of decoding function
"""

def findFunct(search):
    ea = ScreenEA()
    for funcea in Functions(SegStart(ea), SegEnd(ea)):
        name = GetFunctionName(funcea)
        if name == search:
            return funcea
    return None


def decode(bytes):
    a2 = struct.unpack("<L",bytes[:4])[0]
    out = ""
    index = 0
    for i in bytes[4:]:
        a2 = ((a2 << 7) - (a2 >> 3) + index + 0x713a8fc1) & 0xffffffff
        xorbyte = (a2 & 0xff) ^ ((a2 >> 8) & 0xff) ^ ((a2 >> 16) & 0xff) ^ ((a2 > 24) & 0xff)
        out += chr(ord(i) ^ xorbyte)
        index += 1
        return out


def randnst(n):
    import random
    import string
    return "".join([random.choice(string.digits) for x in xrange(n)])


def get_bytes(addr, size):
    if size > 0x100:
        return "Err"
    out = ""
    pos = addr
    end = pos + size
    while pos < end:
        out += chr(idc.Byte(pos))
        pos += 1
    return out


def place_bytes(addr, data, size):
    pos = 0
    while pos < size:
        idc.PatchByte(addr+pos, ord(data[pos]))
        pos += 1
    idc.PatchByte(addr+pos, 0)


def getAndCall(addr, strat = "linear"):

    chain = Walker.Handlers.HandlerChain()
    bits = 32 # <=== bits
    vars = Walker.x86Handlers.BPArgsTracker(bts=bits)
    coll = Walker.x86Handlers.x86Calls([addr], bts=bits)
    basic = Walker.x86Handlers.x86Base(bts=64)
    #Build chain
    chain.addVisitor(vars)
    chain.addVisitor(coll)
    chain.addVisitor(basic)
    walker, conf = Walker.Walker.getMyWalker("xref", strat)     # walk througth all functions that have xrefs to conf['fcn']
    conf['handler'] = chain                                     # and use linear method to process functions (it won't make jmps, just go from begin of function till end)
    conf['fcn'] = findFunct(addr)
    conf['stackargs'] = 2
    conf['jump'] = Walker.x86Handlers.x86_jumps
    runner = walker(conf)
    print "Start walk"
    metadata = runner.walk()
    #!# trace_file.close()
    print "End walk"
    data = ""
    visited = {}
    #This is where processing of collected values should take place
    for j in metadata:
        for i in j:
            try:
                print "%x" % i['pos']
                print i['stack'][0]
                print i['stack'][1]
                data = get_bytes(i['stack'][0], i['stack'][1])
                ret = decode(data)
                if ret != "Err":
                    visited[i['stack'][0]] = (ret, i['stack'][1]-4)
            except Exception as e:
                print e
                #If something goes wrong - just print location
                #in order to determine what's wrong by hand
                print "%x" % i['pos']
    for i in visited:
        elem = visited[i]
        place_bytes(i, elem[0], elem[1])



def decode2(bytes2, size):
    bytes = get_bytes(bytes2, size)
    a2 = struct.unpack("<L",bytes[:4])[0]
    out = ""
    index = 0
    for i in bytes[4:]:
        a2 = ((a2 << 7) - (a2 >> 3) + index + 0x713a8fc1) & 0xffffffff
        xorbyte = (a2 & 0xff) ^ ((a2 >> 8)  & 0xff) ^ ((a2 >> 16) & 0xff) ^ ((a2>>24) &0xff)
        out += chr(ord(i) ^ xorbyte)
        index += 1
    print out