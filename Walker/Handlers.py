import idaapi
import idautils
import idc
from DummyStack import DummyStack
import copy
import traceback
import sys

class HandlerChain(object):

    def __init__(self):
        self.visitors = []

    def addVisitor(self, visitor):
        self.visitors.append(visitor)

    def process(self, mnem, instr):
        for i in self.visitors:
            if i.process(mnem, instr):
                return True
        return False

    def begin(self, walker):
        if len(self.visitors) == 0:
            return
        self.visitors[0].begin(walker)
        for i in self.visitors:
            i.cache(walker)

    def cache(self, walker):
        for i in self.visitors:
            i.cache(walker)

    def end(self):
        if len(self.visitors) > 0:
            return self.visitors[0].end()

class DummyHandler(object):

    def begin(self, walk):
        walk.args['dict']['inner'] = []
        walk.args['dict']['regs'] = {}
        self.cache(walk)

    def cache(self, walk):
        #just cache references to make code a bit cleaner
        #so, after EVERY replacement such fields in walker
        #cache MUST be called
        self._walker = walk
        self._args = walk.args
        self._data = walk.args['data']
        self._dict = walk.args['dict']
        self._regs = self._dict['regs']
        self._stack = self._args['stack']

    def process(self, mnem, instr):
        if hasattr(self, mnem):
            try:
                return getattr(self, mnem)(instr)
            except:
                print "%x: Exception while processing %s" % (instr.ip, mnem)
                print "%s" % self._regs
                traceback.print_exc(file=sys.stdout)
        return False

    def end(self):
        return self._data

    def getRegVal(self, op):
        name = self.getRegName(op)
        if name is not None:
            if name not in self._regs:
                self._regs[name] = 0
            return self._regs[name]
        return "err"

    def getRegName(self, oper):
        return idaapi.get_reg_name(oper.reg, idaapi.get_dtyp_size(oper.dtyp))

class TraceHandler(DummyHandler):

    def __init__(self, fl):
        self.f = fl

    def process(self, mnem, instr):
        z = ("position: %x %s\n regs: %s\n" %(instr.ip, mnem, repr(self._regs)))
        self.f.write(z)
        self.f.flush()
        return False

