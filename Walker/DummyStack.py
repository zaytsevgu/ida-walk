class DummyStack(object):

    def __init__(self, capacity):
        self.data = ["err" for x in xrange(capacity)]
        self.capacity = capacity
        self.curr = 0

    def push(self, data):
        self.curr = (self.curr + 1) % self.capacity
        self.data[self.curr] = data

    def pop(self):
        out = self.data[self.curr]
        self.data[self.curr] = "err"
        self.curr = (self.curr - 1) % self.capacity
        return out

    def get(self, index):
        newcurr = self.curr
        newcurr -= (index)
        newcurr %= self.capacity
        return self.data[newcurr]

    def put(self, index, value):
        newcurr = (self.curr - index) % self.capacity
        self.data[newcurr] = value

    def clear(self):
        self.curr = 0
        self.data = ["err" for i in self.data] #?????

