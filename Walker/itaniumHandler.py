import idaapi
import idautils
import idc
from DummyStack import DummyStack
import copy
idaapi.require("Walker.Handlers")
import Handlers

class itaniumHandler(Handlers.DummyHandler):

    def setRegVal(self, op, value):
        name = self.getRegName(op)
        self._regs[name] = value;

    def __init__(self, gp):
        self.gp = gp

    def begin(self, walk):
        super(itaniumHandler, self).begin(walk)
        self._regs['gp'] = self.gp

    def getVal(self, opnd):
        if opnd.type == idaapi.o_reg:
            return self.getRegVal(opnd)
        elif opnd.type == idaapi.o_imm:
            return opnd.value
        else:
            #Need for debugging
            raise Exception("getVal exception: %x" % opnd.type)

    def add(self, inst):
        if inst.Op1.type == idaapi.o_reg:
            self.setRegVal(inst.Op1, self.getVal(inst.Op2) + self.getVal(inst.Op3))
            return True
        return False

    def adds(self, inst):
        #Well, this commands actually works only with short, reg. But self.add can handle it
        return self.add(inst)

    def mov(self, inst):
        if inst.Op1.type == idaapi.o_reg or inst.Op1.type == idaapi.o_idpspec2: #bN have type o_idpspec2
            self.setRegVal(inst.Op1, self.getVal(inst.Op2))
            return True
        return False

    def ld8(self, inst):
        if inst.Op1.type == idaapi.o_reg:
            #Op2.reg == 24 always for some reason
            #TODO: make it work without such dirty hacks
            addr = self._regs[idc.GetOpnd(inst.ip, 1)[1:-1]]
            print "Got addr %x" % addr
            self.setRegVal(inst.Op1, addr)
            return True
        return False

    def br(self, inst):
        #TODO: find a way to determine instruction suffixes
        if inst.Op2.type == idaapi.o_idpspec2:
            self._data.append({
                           "pos": inst.ip,
                           "regs":copy.copy(self._regs),
                           'type': 'collector call',
                           'val':self.getRegName(inst.Op2),
                          })
            return True
        return False
