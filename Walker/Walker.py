import idaapi
import idautils
import idc
idaapi.require("Walker.DummyStack")
idaapi.require("Walker.Handlers")
import DummyStack
import copy
from abc import ABCMeta, abstractmethod
import Handlers

class Walker:
    __metaclass__ = ABCMeta

    def __init__(self, args):
        self.args = args

    @abstractmethod
    def walk(self):
        pass


class LinearWalker(Walker):

    def walk(self):
        self.args['handler'].begin(self)
        position = self.args['fcn'].startEA
        while position < self.args['fcn'].endEA:
            instr = idautils.DecodeInstruction(position)
            if instr is None:
                print "%x: Not and instruction found" % position
                break
            mnem = instr.get_canon_mnem()
            self.args['handler'].process(mnem, instr)
            position += instr.size
        return self.args['handler'].end()


class JumpWalker(Walker):

    def walk(self):
        self.args['handler'].begin(self)
        position = self.args['fcn'].startEA
        visited = []
        visited_for_jump = []
        startPoints = [(position, self.args['stack'], self.args['dict'])]
        while len(startPoints) > 0:
            position, self.args['stack'], self.args['dict'] = startPoints.pop()
            self.args['handler'].cache(self)
            print "Taking branch %x" % position
            #if position in visited:
            #    print "%x: already visit" % position
            while True: #position not in visited:
                if position not in visited_for_jump:
                    visited_for_jump.append(position)
                instr = idautils.DecodeInstruction(position)
                if instr is None:
                    print "%x :Not and instruction found" % position
                    break
                mnem = instr.get_canon_mnem()
                self.args['handler'].process(mnem, instr)
                if mnem not in self.args['jump']:
                    position += instr.size
                else:
                    #print "%x: Processed jmp: %s" % (position, mnem)
                    if self.args['jump'][mnem] == 1:
                        if position not in visited:
                            #save conditional jump that we already saved
                            #this need to evade infinite loop
                            visited.append(position)
                            startPoints.append((instr.Op1.addr, copy.deepcopy(self.args['stack']), copy.deepcopy(self.args['dict'])))
                        position += instr.size
                    else:
                        if self.args['jump'][mnem] == 0:
                            break
                        if instr.Op1.type == 7:
                            if instr.Op1.addr in visited_for_jump:
                     #           print "%x: Branch eliminated %x -> %x" % (position, position, instr.Op1.addr)
                                break
                            position = instr.Op1.addr
                        else:
                            print "jmp %x" % position
                            break
        return self.args['handler'].end()

class XrefWalker(Walker):

    def getParentFunctions(self):
        saved_names = []
        position = []
        for i in idautils.XrefsTo(self.args['fcn']):
            name = idaapi.get_func(i.frm)
            if name is None:
                continue
            if name not in saved_names:
                saved_names.append(name)
                position.append(i.frm)
        return position

    def walk(self):
        result = []
        for i in  self.getParentFunctions():
            args = copy.copy(self.args)
            args['fcn'] = idaapi.get_func(i)
            args['data'] = []
            result.append(self.args['walker'](args).walk())
        return  result

def getMyWalker(name, params=""):
    if name == "xref":
        return produceXrefWalker(params)
    elif name == "linear":
        return produceLinearWalker()
    elif name == "jump":
        return produceJumpWalker()
    else:
        raise Exception("Error at params")

def getInitConf():
    return {
        'fcn'      : None,
        'stackargs': 0,
        'stack'    : DummyStack.DummyStack(16),
        'data'     : [],
        'dict'     : {},
        'trace'    : False,
        'handler'  : Handlers.DummyHandler
    }

def produceLinearWalker():
    return (LinearWalker, getInitConf())

def produceJumpWalker():
    conf = getInitConf()
    conf['jump'] = {}
    return (JumpWalker, conf)

def produceXrefWalker(name):
    if name == "xref":
        raise Exception("Not today")
    else:
        w, c = getMyWalker(name)
        c['walker'] = w
        return (XrefWalker, c)

#operand.type:
#1 - o_reg
#2 - o_mem
#3 - ...
#4 - o_displ
#5 - o_imm
#6 - o_far
