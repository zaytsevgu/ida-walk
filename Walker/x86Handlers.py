import idaapi
import idautils
import idc
from DummyStack import DummyStack
import copy
idaapi.require("Walker.Handlers")
import Handlers

x86_jumps = {
    'jmp': 2,
    'jnz': 1,
    'jz' : 1,
    'jne': 1,
    'je' : 1,
    'jge': 1,
    'jg' : 1,
    'jl' : 1,
    'jle': 1,
    'js' : 1,
    'ja' : 1,
    'jb' : 1,
    'jns': 1,
    'jnb': 1,
    'jna': 1,
    'ret': 0,
    'retn':0
}

register_adapter = {
    'al':   [0 ,0,8],
    'cl':   [1 ,0,8],
    'dl':   [2 ,0,8],
    'bl':   [3 ,0,8],
    'spl':  [4 ,0,8],
    'bpl':  [5 ,0,8],
    'ah':   [0 ,8,8],
    'ch':   [1 ,8,8],
    'dh':   [2 ,8,8],
    'bh':   [3 ,8,8],
    'ax':   [0 ,0,16],
    'cx':   [1 ,0,16],
    'dx':   [2 ,0,16],
    'bx':   [3 ,0,16],
    'sp':   [4 ,0,16],
    'bp':   [5 ,0,16],
    'sil':  [6 ,0,8],
    'dil':  [7 ,0,8],
    'si':   [6 ,0,16],
    'di':   [7 ,0,16],
    'r8d':  [8 ,0,32],
    'r9d':  [9 ,0,32],
    'r10d': [10,0,32],
    'r11d': [11,0,32],
    'r12d': [12,0,32],
    'r13d': [13,0,32],
    'r14d': [14,0,32],
    'r15d': [15,0,32],
    'r8w':  [8 ,0,16],
    'r9w':  [9 ,0,16],
    'r10w': [10,0,16],
    'r11w': [11,0,16],
    'r12w': [12,0,16],
    'r13w': [13,0,16],
    'r14w': [14,0,16],
    'r15w': [15,0,16],
    'r8b':  [8 ,0,8],
    'r9b':  [9 ,0,8],
    'r10b': [10,0,8],
    'r11b': [11,0,8],
    'r12b': [12,0,8],
    'r13b': [13,0,8],
    'r14b': [14,0,8],
    'r15b': [15,0,8],
    'eax' : [0 ,0,32],
    'ecx' : [1 ,0,32],
    'edx' : [2 ,0,32],
    'ebx' : [3 ,0,32],
    'esp' : [4 ,0,32],
    'ebp' : [5 ,0,32],
    'esi' : [6 ,0,32],
    'edi' : [7 ,0,32],
}


class x86Handler(Handlers.DummyHandler):

    class fakeOp(object):

        def __init__(self, reg, dtyp):
            self.reg = reg
            self.dtyp = dtyp

    regs64 = {
        0: "rax",
        1: "rcx",
        2: "rdx",
        3: "rbx",
        4: "rsp",
        5: "rbp",
        6: "rsi",
        7: "rdi",
        8: "r8",
        9: "r9",
        10: "r10",
        11: "r11",
        12: "r12",
        13: "r13",
        14: "r14",
        15: "r15",
        16: "al",
        17: "cl",
        18: "dl",
        19: "bl",
        26: "sil",
        27: "dil",
    }

    regs32 = {
        0: "eax",
        1: "ecx",
        2: "edx",
        3: "ebx",
        4: "esp",
        5: "ebp",
        6: "esi",
        7: "edi",
        8: "8",
        9: "9",
        16: "al",
        17: "cl",
        18: "dl",
        19: "bl",
        20: "ah",
        21: "ch",
        22: "dh",
        23: "bh",
        24: "spl",
        25: "bpl",
        26: "sil",
        27: "dil",
    }

    def __init__(self, bts=32):
        self._bts = bts
        if bts == 32:
            self.regs = x86Handler.regs32
        elif bts == 64:
            self.regs = x86Handler.regs64
        else:
            raise Exception("WHAT?")
        self.addr = idc.Dword
        if bts == 64:
            self.addr = idc.Qword

    def setRegVal(self, reg_op,  value):
        name = self.getRegName(reg_op)
        if name not in register_adapter:
            self._regs[name] = value
        else:
            adap = register_adapter[name]
            fake_op = x86Handler.fakeOp(adap[0], idaapi.get_dtyp_by_size(self._bts//8))
            target_name = self.getRegName(fake_op)
            mask = (1 << adap[2]) - 1
            if type(value) == str:
                self._regs[target_name] = value
                return
            if target_name not in self._regs or type(self._regs[target_name]) == str:
                self._regs[target_name] = (value & (mask << adap[1]))
                #print "Replacing err by value from register part!" #too much flood :(
                return
            val = self._regs[target_name]
            val = val & ~((mask << adap[1])) # set null part
            val = val | ((value & mask) <<adap[1])
            self._regs[target_name] = val


    def addMovMemImm(self, instr):
        if instr.Op1.type == idaapi.o_mem:
            if instr.Op2.type == idaapi.o_imm:
                return {"dst":instr.Op1.addr, "val":instr.Op2.value}
        return None

    def addMovMemReg(self, instr, reg=-1):
        if instr.Op1.type == idaapi.o_mem:
            if instr.Op2.type == idaapi.o_reg:
                if reg == -1 or instr.Op2.reg == reg:
                    return {"dst":instr.Op1.addr, "val": instr.Op2}
        return None

    def addMovRegMem(self, instr, reg=-1):
        if instr.Op1.type == idaapi.o_reg:
            if instr.Op2.type == idaapi.o_mem:
                if reg == -1 or instr.Op1.reg == reg:
                    return {"dst":instr.Op1, "val": instr.Op2.addr}

    def addMovDisplReg(self, instr, reg):
        if instr.Op1.type == idaapi.o_displ:
            if instr.Op2.type == idaapi.o_reg:
                if reg == -1 or instr.Op2.reg == reg:
                    return {"dst_r":instr.Op1, "offs": instr.Op1.addr,
                            "val": instr.Op2
                           }

    def addMovRegDispl(self, instr, reg):
        if instr.Op1.type == idaapi.o_reg:
            if instr.Op2.type == idaapi.o_displ:
                if reg == -1 or instr.Op2.reg == reg:
                    return {"val":instr.Op1, "offs": instr.Op2.addr,
                            "src_r": instr.Op2
                           }

    def saveCall(self, instr, name):
        self._data.append({"name":name,
                           "pos": instr.ip,
                           "regs":copy.copy(self._regs),
                           'stack':[self._stack.get(i) for i in
                                    xrange(self._args['stackargs'])],
                           'type': 'collector call'
                          })


    @staticmethod
    def isReg(oper, reg=-1):
        if oper.type == idaapi.o_reg:
            if reg == -1 or oper.reg == reg:
                return True
        return False

    def getAddr(self,oper, inst):
        if oper.type == idaapi.o_mem:
            return oper.addr
        if oper.type == idaapi.o_displ:
            regval = self.getRegVal(oper)
            if type(regval) != str:
                return regval + oper.addr
        return "err"

    @staticmethod
    def getImm(oper):
        if oper.type == idaapi.o_imm:
            return oper.value
        return "err"

    def getCallAddr(self, oper):
        if oper.type == idaapi.o_near or oper.type == idaapi.o_far:
            return oper.addr
        if oper.type == idaapi.o_reg:
            return self.getRegVal(oper)
        if oper.type == idaapi.o_mem:
            ######################
            # Need investigation
            # on PE files we should treat addr as value
            # not as pointer
            # this is ugly and not generic fix
            ######################
            if self.addr(oper.addr) == idaapi.BADADDR:
                return oper.addr
            return self.addr(oper.addr)
        return "err"

    def getOperand(self, oper):
        val = x86Handler.getImm(oper)
        if val == "err": #ugly
            return self.getCallAddr(oper)
        return val

    @staticmethod
    def isGccStylePush(oper):
        if oper.type == idaapi.o_displ or oper.type == idaapi.o_phrase:
            if oper.reg == 4:
                if oper.addr < 48 and oper.addr % 4 == 0:
                    return True
        return False


class x86Base(x86Handler):

    def push(self, instr):
        typeOp = instr.Op1.type
        if typeOp == idaapi.o_imm:
            self._stack.push(instr.Op1.value)
        elif typeOp == idaapi.o_mem: #is this possible?
            self._stack.push(instr.Op1.addr)
        elif typeOp == idaapi.o_reg:
            self._stack.push(self.getRegVal(instr.Op1))
        else:
            self._stack.push("err")
        return True

    def pop(self, instr):
        data = self._stack.pop()
        if x86Handler.isReg(instr.Op1):
            self.setRegVal(instr.Op1, data)
        return True

    def mov(self, instr):
        if x86Handler.isReg(instr.Op1):
            val = self.addMovRegMem(instr)
            if val is not None:
                self.setRegVal(val['dst'], self.addr(val['val']))
                return True
            if x86Handler.isReg(instr.Op2):
                self.setRegVal(instr.Op1, self.getRegVal(instr.Op2))
                return True
            self.setRegVal(instr.Op1,self.getImm(instr.Op2))
            return True
        return False

    def inc(self, instr):
        if x86Handler.isReg(instr.Op1):
            try:
                reg = self.getRegVal(instr.Op1)
                reg += 1
                self.setRegVal(instr.Op1, reg)
            except:
                print "%x: Error at inc!" % instr.ip
                print "Register values "+str(self._regs)
        return True

    def xor(self, instr):
        if x86Handler.isReg(instr.Op1):
            xor = 0
            if x86Handler.isReg(instr.Op2):
                if instr.Op1.reg == instr.Op2.reg:
                    self.setRegVal(instr.Op1, 0)
                    return True
                xor = self.getRegVal(instr.Op2)
            else:
                xor = self.getImm(instr.Op2)
            if xor == "err":
                print "%x: Xor error" % instr.ip
            else:
                try:
                    self.setRegVal(instr.Op1, self.getRegVal(instr.Op1) ^ xor)
                    return True
                except:
                    print "%x: Can't xor %s with %s" % (instr.ip, str(self.getRegVal(instr.Op1)), str(xor))
        return False

    def lea(self, instr):
        adr = self.getAddr(instr.Op2, instr)
        if x86Handler.isReg(instr.Op1):
            self.setRegVal(instr.Op1, adr)
            return True
        return False


class x86Calls(x86Handler):

    def __init__(self, functions, bts=32):
        super(x86Calls, self).__init__(bts)
        self.fcns = functions

    def mov(self, instr):
        if x86Handler.isGccStylePush(instr.Op1):
#            print "%x gcc_push" % instr.ip
#            print self.getOperand(instr.Op2)
            self._stack.put((instr.Op1.addr // (self._bts/8)), self.getOperand(instr.Op2))
            return True
        return False

    def call(self, instr):
	#print "Call from basic visitor %x" % instr.ip
        call_addr = self.getCallAddr(instr.Op1)
	#print "%x" % call_addr
        if call_addr != "err":
            name = idc.GetFunctionName(call_addr)
            if name in self.fcns:
                self.saveCall(instr, name)
                return True
        return False


class Variable:

        def __init__(self):
            self.size = 0
            self.offset = 0
            self.value = "err"


class BPArgsTracker(x86Handler):


    def __init__(self, hooks = {}, bts=32):
        super(BPArgsTracker, self).__init__(bts)
        self.vars = []
        self.hooks = hooks

    def getVar(self,index):
        for i in self.vars:
            if i.offset == index:
                return i.value
        return "err"

    def saveVar(self, index, value):
        for i in self.vars:
            if i.offset == index:
                i.value = value
                return
        var = Variable()
        var.offset = index
        var.value = value
        self.vars.append(var)
        return

    def mov(self, instr):
        if instr.Op1.type == idaapi.o_displ and instr.Op1.reg == 5:
            if instr.Op2.type == idaapi.o_reg:
                data = self.getRegVal(instr.Op2)
            else:
                data = self.getImm(instr.Op2)
            self.saveVar(instr.Op1.addr, data)
            return True
        else:
            val = self.addMovRegDispl(instr, 5)
            if val is not None:
                self.setRegVal(val['val'], self.getVar(val['offs']))
                return True
        return False

    def lea(self, instr):
        val = self.addMovRegDispl(instr, 5)
        if val is not None:
            self.setRegVal(val['val'], "addr_"+str(val['offs']))
            return True
        return False

    def call(self, instr):
        call_addr = self.getCallAddr(instr.Op1)
#        print "Call %x" % instr.ip
        if call_addr != "err":
            name = idc.GetFunctionName(call_addr)
 #           print name
            if name in self.hooks.keys():
                return self.hooks[name](self)
        return False


class GlobalVarTracker(x86Handler):

    def __init__(self, bts=32):
        super(GlobalVarTracker, self).__init__(bts)
        self._glob = {}

    def getVar(self, addr):
        return self._glob[addr]

    def saveVar(self, addr, value):
        self._glob[addr] = value

    def mov(self, instr):
        val = self.addMovMemReg(instr);
        if val is None:
            val = self.addMovRegMem(instr)
            if val is not None and val['val'] in self._glob:
                self.setRegVal(val['dst'], self._glob[val['val']])
                return True
            return False
        self.saveVar(val['dst'], self.getRegVal(val['val']))

class AddrCallTrack(x86Handler):

    def __init__(self, addr = [], bts=32):
        super(AddrCallTrack, self).__init__(bts)
        self.addrz = addr

    def call(self, instr):
	#print "Call from addr tracker %x" % instr.ip
        call_addr = self.getCallAddr(instr.Op1)
	#print "%x" % call_addr
        if call_addr in self.addrz:
            self.saveCall(instr, "%x" % call_addr)
            return True
        return False
