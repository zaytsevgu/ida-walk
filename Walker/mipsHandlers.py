import idaapi
import idautils
import idc
from DummyStack import DummyStack
import copy
import Handlers

class mipsHandler(Handlers.DummyHandler):

    regs = {
        0: "zero",
        2: "v0",
        3: "v1",
        4: "a0",
        5: "a1",
        6: "a2",
        7: "a3",
        8: "t0",
        9: "t1",
        10: "t2",
        11: "t3",
        16: "s0",
        17: "s1",
        18: "s2",
        19: "s3",
        20: "s4",
        21: "s5",
        22: "s6",
        23: "s7",
        24: "24",
        25: "t9",
        28: "gp",
        29: "sp",
        30: "fp",
        31: "ra",
    }


class mipsBase(mipsHandler):

    def la(self, inst):
        if inst.Op2.type == idaapi.o_imm: #maybe not needed
            self._regs[self.getRegName(inst.Op1)] = inst.Op2.value
            return True
        return False

    def lw(self, inst):
        try:
            if inst.Op2.type == idaapi.o_displ:
                self._regs[self.getRegName(inst.Op1)] = idc.Dword(inst.Op2.addr + self.getRegVal(inst.Op2))
                return True
        except:
            print "%x: Error at lw Regs:\n%s" % (inst.ip, repr(self.regs))
        return False

    def move(self, inst):
        op1 = inst.Op1
        op2 = inst.Op2
        self._regs[self.getRegName(op1)] = self.getRegVal(op2)
        return True


    def addiu(self, inst):
        op1 = inst.Op1
        op2 = inst.Op2
        op3 = inst.Op3
        try:
            if op2.type == idaapi.o_imm:
                self._regs[self.getRegName(op1)] = self.getRegVal(op1) + op2.value
            if op3.type == idaapi.o_imm:
                self._regs[self.getRegName(op1)] = self.getRegVal(op2) + op3.value
                return True
            if op3.type == idaapi.o_reg:
                self._regs[self.getRegName(op1)] = self.getRegVal(op2) + self.getRegVal(op3)
                return True
        except:
            print "%x: error Regs:\n%s" %(inst.ip, repr(self._regs))
        return False


class mipsCollector(mipsHandler):

    def __init__(self, functions):
       self.fcns = functions

    def jalr(self, inst):
        if inst.Op1.type == idaapi.o_reg:
            addr = self.getRegVal(inst.Op1)
            name = idc.GetFunctionName(addr)
            if name in self.fcns:
                self._data.append({"name": name,
                                     "pos" : inst.ip,
                                     "regs": copy.copy(self._dict["regs"]),
                                     "type": "collector call"
                                  })
            return True
        return False


