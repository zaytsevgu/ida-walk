import idaapi
import idautils
import idc
import Walker
idaapi.require("Walker")
idaapi.require("Walker.Walker")
idaapi.require("Walker.Handlers")
idaapi.require("Walker.x86Handlers")
from array import array
"""
Example usage of Walker module
"""


def findFunct(search):
    ea = ScreenEA()
    for funcea in Functions(SegStart(ea), SegEnd(ea)):
        name = GetFunctionName(funcea)
        if name == search:
            return funcea
    return None

def getAndCall(addr, strat = "linear"):
    """ Takes address of "_objc_msgSend" symbol and set up comment with selector value
        at call place. Works on 64 bits Mach-O. In order to make it works with 32-bit binaries
        need to change bts values and processing method
    """
    chain = Walker.Handlers.HandlerChain()
    bits = 64 # <=== bits
    #!# trace_file = open("trace.txt","w")                          # This may produce a huge log file, but mostly it use for debugging visitors
    #!# tracer = Walker.Handlers.TraceHandler(trace_file)           # If traces will be last in chain it will be logging only unprocessed instructions
    vars = Walker.x86Handlers.BPArgsTracker(bts=bits)
    send = Walker.x86Handlers.AddrCallTrack(addr=[addr], bts=bits)
    coll = Walker.x86Handlers.x86Calls(["_objc_msgSend"], bts=bits)
    basic = Walker.x86Handlers.x86Base(bts=64)
    #Build chain
    #!# chain.addVisitor(tracer)
    chain.addVisitor(vars)
    chain.addVisitor(send)
    chain.addVisitor(coll)
    chain.addVisitor(basic)
    walker, conf = Walker.Walker.getMyWalker("xref", strat)     # walk througth all functions that have xrefs to conf['fcn']
    conf['handler'] = chain                                     # and use linear method to process functions (it won't make jmps, just go from begin of function till end)
    conf['fcn'] = addr
    conf['stackargs'] = 0 # <- value by default. If function's arguments located at the stack should be non-zero
    conf['jump'] = Walker.x86Handlers.x86_jumps
    runner = walker(conf)
    print "Start walk"
    metadata = runner.walk()
    #!# trace_file.close()
    print "End walk"
    data = ""
    #This is where processing of collected values should take place
    for j in metadata:
        for i in j:
            try:
                #print i
                if i['regs']['rsi'] == "err":
                    print "%x" % i['pos']
                    continue
                meth = GetString(i['regs']['rsi'])
                MakeComm(i['pos'], meth)
            except:
                #If something goes wrong - just print location
                #in order to determine what's wrong by hand
                print "%x" % i['pos']
